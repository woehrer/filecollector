from tkinter import *
from tkinter import messagebox, filedialog

import helper.config
import helper.functions
import windows.info

conf = helper.config.initconfig()
helper.functions.initlogger()

# Ein Fenster erstellen
fenster = Tk()
# Den Fenstertitle erstellen
fenster.title("File Collector")

# Menüleiste erstellen 
menuleiste = Menu(fenster)

# Menü Datei und Help erstellen
datei_menu = Menu(menuleiste, tearoff=0)
help_menu = Menu(menuleiste, tearoff=0)

datei_menu.add_command(label="Config", command=windows.info.messagebox)
datei_menu.add_separator()
datei_menu.add_command(label="Exit", command=fenster.quit)

help_menu.add_command(label="Info", command=windows.info.messagebox)

menuleiste.add_cascade(label="File", menu=datei_menu)
menuleiste.add_cascade(label="Help", menu=help_menu)

fenster.config(menu=menuleiste)   

Path_Excel_label = Label(fenster, text = conf['pathexcel'])
Path_Search_label = Label(fenster, text = conf['pathsearch'])
Path_Collect_label = Label(fenster, text = conf['pathcollect'])

# Stückliste wählen
def openfileexcel():
    Path = filedialog.askopenfilename(filetypes=[('Excel files', '*.xlsx')])
    helper.config.setpathexcel(Path)
    Path_Excel_label.config(text=Path)

openfile_excel_Button = Button(fenster, text = "Stückliste wählen", command=openfileexcel)

# Suchordner wählen
def openfilesearch():
    Path = filedialog.askdirectory()
    helper.config.setpathsearch(Path)
    Path_Search_label.config(text=Path)

openfile_search_Button = Button(fenster, text = "Suchordner wählen", command=openfilesearch)

# Zielordner wählen
def openfilecollect():
    Path = filedialog.askdirectory()
    helper.config.setpathcollect(Path)
    Path_Collect_label.config(text=Path)

openfile_collect_Button = Button(fenster, text = "Zielordner wählen", command=openfilecollect)


# Suche in Excel nach Nummern
def searchinexcel():
    helper.functions.searchinexcel(statusbar,conf['pathexcel'])
excel_Button = Button(fenster, text = "Suchen in Stückliste", command=searchinexcel)

# Suche die Dateien in den Ordnern
def searchfiles():
    helper.functions.searchfiles(statusbar,conf['pathsearch'])

search_Button = Button(fenster, text = "Suchen", command=searchfiles)

# Sammle die Dateien aus den Ordnern
def collectfiles():
    helper.functions.copyfiles(statusbar,conf['pathcollect'])

collect_Button = Button(fenster, text = "Sammeln", command=collectfiles)


statusbar = Label(fenster, text="", bd=1, relief=SUNKEN, anchor=W)

openfile_excel_Button.pack()
Path_Excel_label.pack()
openfile_search_Button.pack()
Path_Search_label.pack()
openfile_collect_Button.pack()
Path_Collect_label.pack()
excel_Button.pack()
search_Button.pack()
collect_Button.pack()
statusbar.pack(side=BOTTOM, fill=X)

def on_closing():
    if messagebox.askokcancel("Quit", "Do you want to quit?"):
        fenster.destroy()

fenster.protocol("WM_DELETE_WINDOW", on_closing)

fenster.geometry("300x250")

fenster.mainloop()