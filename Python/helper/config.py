import configparser
import logging
import sys
import os
from tkinter import messagebox

config = configparser.ConfigParser()
configDir = "./config"
configFile = "config/config.ini"

def initconfig():
    if not os.path.isdir(configDir):
        os.mkdir(configDir)
    if os.path.isfile(configFile):
        pass
    else:
        create()
    #Konfigdatei initialisieren
    try:
        #Config Datei auslesen
        config.read(configFile)
        conf = config['DEFAULT']
        return conf
    except:
        messagebox.showerror(message="Error while loading the Config file" , title = "Error")
        print("Error while loading the Config file:" + str(sys.exc_info()))
        logging.error("Error while loading the Config file" + str(sys.exc_info()))

def create():
    
    config['DEFAULT'] = {'Pathexcel' : '',
                         'Pathsearch' : '',
                         'Pathcollect' : '',
                        }

    with open(configFile, 'w') as configfile:
        config.write(configfile)

def setpathexcel(text):
    config.set('DEFAULT','Pathexcel',str(text))
    with open('config/config.ini', 'w') as configfile:
        config.write(configfile)

def setpathsearch(text):
    config.set('DEFAULT','Pathsearch',str(text))
    with open('config/config.ini', 'w') as configfile:
        config.write(configfile)

def setpathcollect(text):
    config.set('DEFAULT','Pathcollect',str(text))
    with open('config/config.ini', 'w') as configfile:
        config.write(configfile)