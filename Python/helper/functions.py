import logging
from tkinter import messagebox
import openpyxl
from openpyxl import load_workbook
import glob
import os
import shutil
from multiprocessing import Process

def initlogger():
    if not os.path.isdir("./logs"):
        os.mkdir("./logs")
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    handler = logging.FileHandler("logs/ADIFCheck.log")
    formatter = logging.Formatter('%(asctime)s:%(levelname)s-%(message)s')
    handler.setFormatter(formatter)
    handler.setLevel(logging.INFO)
    logger.addHandler(handler)

def searchinexcel(statusbar,path):
    liste = []
    searchword = "Zeichnungsnummer"

    # Lade Excel Datei
    try:
        wb = load_workbook(path)
    except openpyxl.utils.exceptions.InvalidFileException:
        messagebox.showerror("Fehler", "Die Datei konnte nicht geladen werden.\n" + path)
        logging.error("Die Datei konnte nicht geladen werden.\n" + path)
        return

    # Öffne Tabelle 1
    try:
        ws = wb['Tabelle1']
        for row in ws.iter_rows():
            for cell in row:
                if cell.value == searchword:
                    find_row = cell.row
                    find_colum = cell.column
    except:
        messagebox.showerror("Fehler", "Tabelle1 in Excel nicht gefunden.\n" + path)
        logging.error("Tabelle1 in Excel nicht gefunden.\n" + path)
        return
    
    # Suche Keyword in Tabelle
    if not 'find_row' in locals() or not 'find_colum' in locals():
        messagebox.showerror("Fehler", "Die Stückliste enthält kein: " + searchword)
        logging.error("Die Stückliste enthält kein: " + searchword)
        return 
    
    # Lese alle Zeilen unter dem Keyword
    for row in ws.iter_rows(min_col=find_colum, max_col=find_colum, min_row=find_row+1):
        for cell in row:
            liste.append(str(cell.value))
    
    # Schreibe Liste in Textdatei
    listetxt = open("liste.txt", "w")
    for list in liste:
        listetxt.write(str(list) + "\n")
    listetxt.close()

    # Schreibe Statusmeldungen
    text = str(len(liste)) + " Zeichnungsnummern gefunden\n"
    for texte in liste:
        text = text + texte + "\n"
    statusbar.config(text=str(len(liste)) + " Zeichnungsnummern gefunden")
    logging.debug(liste)
    messagebox.showinfo("Liste", text)

def searchfiles(statusbar,path):
    fileliste = []
    # Lade Liste und lösche Inhalt
    filelistetxt = open("fileliste.txt", "w")
    filelistetxt.write("")
    filelistetxt.close()
    filelistnotfound = open("filelistnotfound.txt", "w")
    filelistnotfound.write("")
    filelistnotfound.close()

    # Lade Liste
    listetxt = open("liste.txt", "r")
    liste = listetxt.read().splitlines()
    listetxt.close()

    # Checken ob Liste leer ist
    if len(liste) == 0:
        messagebox.showerror("Fehler", "Es wurde keine Stückliste gescannt oder keine Nummern gefunden.")
        return
    else:
            
        # Suche Dateien
        for name in liste:
            statusbar.config(text=str(liste.index(name))+ "/" + str(len(liste)))
            formats = ['pdf','SLDPRT']
            for format in formats:
                pathname = path + "\\**\\*" + name + "." + format
                files = glob.glob(pathname, recursive=True)
                if len(files) > 0:
                    index = open("index.txt", "a")
                    for file in files:
                        index.write(file + "\n")
                    index.close()
                    fileliste.append(files[0])
                    filelistetxt = open("fileliste.txt", "a")
                    filelistetxt.write(files[0] + "\n")
                    filelistetxt.close()
                    logging.debug("Datei gefunden: " + files[0])
                else:
                    filelistnotfound = open("filelistnotfound.txt", "a")
                    filelistnotfound.write(name + "\n")
                    filelistnotfound.close()
                    logging.error("Die Datei konnte nicht gefunden werden. " + str(liste.index(name))+ "/" + str(len(liste)) + " " + name)
            
        
        # Gebe nicht gefundene aus        
        filelistnotfound = open("filelistnotfound.txt", "r")
        notfound = filelistnotfound.read().splitlines()
        filelistnotfound.close()
        if notfound != "":
            text = ""
            for texte in notfound:
                text = text + texte + "\n"
            messagebox.showerror("Fehler", "Die folgenden Dateien konnten nicht gefunden werden:\n" + text)
        
        # Gebe Statusmeldungen aus
        text = ""
        for texte in fileliste:
            text = text + texte + "\n"
        messagebox.showinfo("Dateien", text)
        logging.debug(fileliste)
        statusbar.config(text="Dateien suchen fertig")

def copyfiles(statusbar, path):
    # Lade Liste
    filelistetxt = open("fileliste.txt", "r")
    fileliste = filelistetxt.read().splitlines()
    filelistetxt.close()
    if len(fileliste) == 0:
        messagebox.showerror("Fehler", "Es wurden keine Dateien zum kopieren gefunden.")
    else:
        for file in fileliste:
            statusbar.config(text=str(fileliste.index(file))+ "/" + str(len(fileliste)))
            if not os.path.isdir(path):
                os.mkdir(path)
            try:
                shutil.copy(file, path)
            except shutil.SameFileError:
                messagebox.showerror("Fehler", "Die Datei ist bereits vorhanden.\n" + file)
                logging.error("Die Datei ist bereits vorhanden.\n" + file)
            except shutil.Error:
                messagebox.showerror("Fehler", "Die Datei konnte nicht kopiert werden.\n" + file)
                logging.error("Die Datei konnte nicht kopiert werden.\n" + file)
            except PermissionError:
                messagebox.showerror("Fehler", "Die Datei konnte nicht kopiert werden.\nKeine Berechtigung" + file)
                logging.error("Die Datei konnte nicht kopiert werden.\nKeine Berechtigung" + file)
            except:
                messagebox.showerror("Fehler", "Die Datei konnte nicht kopiert werden.\n" + file)
                logging.error("Die Datei konnte nicht kopiert werden.\n" + file)
            logging.info("Die Datei wurde kopiert: " + str(fileliste.index(file))+ "/" + str(len(fileliste)) + " " + file)
        messagebox.showinfo("Dateien", "Dateien kopieren fertig.")
        logging.info("Dateien koieren fertig.")
        statusbar.config(text="Dateien kopieren fertig.")
